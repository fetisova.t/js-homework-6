/**
 * Created on 23.06.2019.
 */
//Опишите своими словами как работает цикл forEach.
//Цикл for each перебирает массив и выполнет заданный набор действий с каждым элементом массива, но НЕ возвращает ничего (в отличии от метода map, который делает то же самое, но имеет возвращаемое значение)
let arr = ['string', null, undefined,[1,1,1,3],{name:'name', value:'Gogi'},12, true];
function filterBy(array, excludeType) {
    let newArr = [];
    array.forEach(function (item,i, arr){
        if(excludeType===null){
            if(item!== excludeType){
                    newArr.push(item);
            }
        }else if(excludeType==='object' && item===null){
                newArr.push(item);
        }else if(typeof item!== excludeType){
                    newArr.push(item);
        }
    });
    return newArr;
}
console.log(filterBy(arr, 'undefined'));